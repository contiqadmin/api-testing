var frisby = require('frisby');

/* Config Options with defaults */
var emaillogin = process.env['emaillogin'];
var password = process.env['password'];
var base_url = process.env['base_url'];

// this is for future work.  Right now we grab the first project in the user's list of projects for the test
var a_project_id = process.env['project_id'];


if (base_url == null){  
  base_url = 'https://test.contiq.com';
}
if (emaillogin == null){  
    emaillogin = 'graham@chiseldesign.com';
}
if (password == null){  
  password = 'grahammyhre';
}
/* end config options with defaults */

frisby.create('Test login and get an Auth Token for all other tests')
  .post(base_url + '/api/authenticate/userpass?username=' + emaillogin + '&password=' + password +'', { some: 'test data' }, { strictSSL: false })
  //.inspectRequest()
  .inspectJSON()
  .expectStatus(200)
  .expectHeaderContains('content-type', 'application/json')
  // examples
  // .expectJSON('0', {
  //   posts: function(val) { expect(val).toMatchOrBeNull("Oklahoma City, OK"); }, // Custom matcher callback
  //   user: {
  //     verified: false,
  //     location: "Oklahoma City, OK",
  //     url: "http://brightb.it"
  //   }
  // })
  // .expectJSON({
  //     token:
  //     {
  //       title: "image from : 64 - brad@betterbrushes.com"
  //     }
  //   })
  .expectJSONTypes({
    token: String,
    expiresOn: String 
  })
  .afterJSON(function(response) {
    console.log("\n did we get a response auth token? :: " + response.token); // TESTING
    
    // 
    // Include API KEY in the header of all future requests
    //
    frisby.globalSetup({
    request: {
      headers: {
        'X-Auth-Token': response.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        // add a ten second timeout for the export function
        
       }
     }
   });
   
   
  // 
  // Run all subsequent tests that need the AUTH TOKEN
  //


   //
   // test crs search
   //
   
   frisby.create('Search CRS')
     .get(base_url + '/api/crs/search?query=cloud', { strictSSL: false })

     .inspectRequest()
     .expectStatus(200)
     .inspectJSON()
   .toss();

    //
    // Create A New Project
    //
   // GM-PUT
  frisby.create('create a new project')
    .post(base_url + '/api/projects', { 
		name: "a test project", 
		description: "a description", 
		type: "presentation"
	}, {json: true, strictSSL: false })
    // .inspectRequest()
    .expectStatus(201)
  .toss()


   
   //
   // test getting a list of projects
   //
   frisby.create('GET All project for this user')
     .get(base_url + '/api/projects', { strictSSL: false } , { some: 'test data' })
      //.inspectRequest()
      .expectStatus(200)
      //.inspectJSON()
      // .expectHeaderContains('Content-Type', 'json')
      //.expectJSON({ /* whatever data you expect */ })
      //.inspectJSON()
      .afterJSON(function(json) {
        //console.log("\n did we get a first id response from a list of projects :: " + json[0].id); // TESTING
        a_project_id = json[0].id;
        console.log("\n An ID of a project :: " + a_project_id); // TESTING


       //
       // RUNNING ALL SUBSEQUENT TESTS THAT NEED A SPECIFIC PROJECT ID
       ///A group of tests that need a project ID, the first test is to show more info
          

          
      //
      // Export a project
      // GM-PUT
        
      //  NOTE: EXPORT A PROJECT REQUIRES StrictSSL false in this case!
      console.log("Exporting a project");
      frisby.create('Export a project')
       .post(base_url + '/api/projects/' + a_project_id + '/export', { test: 'nothing' }, { strictSSL: false })
        .removeHeader('Content-Type')
        .removeHeader('accept')        
        .addHeaders({'Content-Type': 'application/octet-stream'})
        //.inspectRequest()
        .expectStatus(200)
        //.inspectHeaders()
        //.inspectBody()
        //.expectHeaderContains('This one is a file of type ppt or something like that')
        //.expectJSON()
        //.inspectJSON()
      .toss();
      
      
      //
      // Add to Storyline
      //
      // the data is some random data, need to learn more about it.   
      //GM-PUT
      frisby.create('Add To Storyline Test')
        .post(base_url + '/api/projects/' + a_project_id +'/clipboard', {"data":[{"title":"","order":1,"slides":[{"slideID":"6-20","slideOrder":0,"slideNumber":"20"},{"slideID":"6-20","slideOrder":0,"slideNumber":"20"}]}]}, {json: true, strictSSL: false })
        //.inspectRequest()
        .expectStatus(200)
        // write the test
        // .inspectJSON()
      .toss();




      //
      // GetViewer Info
      //
      // This test fails

      // console.log("\n Getting viewer info");
      // 'Get Viewer Info /api/projects/<projectId>/viewerInfo')
      // console.log("")
      frisby.create('Get Viewer Info /api/projects/' + a_project_id + '/viewerInfo')
        .get(base_url + '/api/projects/' + a_project_id + '/viewerInfo', {strictSSL: false })

        // .inspectRequest()
        .expectStatus(200)

        //.inspectRequest()
        //.expectStatus(200)
        // .inspectJSON()
      .toss();





      
      
      //*****//
      //  This is the ned of the project_id section
      //*****//
       
      }) // afterjson function for getting a list of projects
      
    .toss();
  
  

    //
    // SEARCH CRS
    //
    // This one needs the strictSSL false also
    frisby.create('Search CRS')
      .get(base_url + '/api/crs/search?query=test',  { strictSSL: false }, {json: true})
      //.inspectRequest()
      .expectStatus(200)

      //.inspectHeaders()
      .inspectJSON()
      //.expectHeaderContains('Content-Type', 'json')
      .expectJSONTypes('data', {
        docs: Array
      })
    
      //.inspectJSON()
      // add a test to make sure the thumbnail url is in the picture
      
    .toss();


    //
    // Get Files for this users.  Used in My Content
    //
    //
    frisby.create('Get Files For A User')
      .get(base_url + '/api/resources/files-only', { strictSSL: false })
      // .inspectRequest()
      .expectStatus(200)
      // .inspectJSON()
      .afterJSON(function(json) {
        //*****//
        //  Begin Resource AKA FILE DEPENDENT SECTION OF SITE
        //*****//
      
        a_resource_id = json.data[0].id;
        console.log("\n An Id of a File AKA a Resource :: " + a_resource_id); 
      
        //
        // Get slides from that files... Dependent on resource_id
        //
      
        frisby.create('Get Slides')
          .get(base_url + '/api/resources/' + a_resource_id + '/slides', { strictSSL: false })
          .expectStatus(200)
          // .inspectRequest()
          //.inspectJSON()
        .toss();


        //
        // Share a resource
        //
        // This one fails without a valid email in the system
        frisby.create('Testing Share A Resource With Somebody')
          .post(base_url + '/api/resources/' + a_resource_id + '/shared', { toEmail: 'rahul@contiq.com' }, {json: true, strictSSL: false })
          .expectStatus(200)
          // .inspectStatus()
          //.inspectRequest()
          //.inspectBody()
        .toss();
        
      
      
      
      
      
        }) // end After JSON
        //*****//
        //  end Resource AKA FILE DEPENDENT SECTION OF SITE
        //*****//
    .toss();  // the toss for getting a file ID test


    frisby.create('Get All Users')
      .get(base_url + '/api/users', { strictSSL: false })
      .expectStatus(200)
      // .inspectRequest()
      // .inspectJSON()
    .toss();

    frisby.create('Get A User Information Objects')
      .get(base_url + '/api/users/me', { strictSSL: false })
      .expectStatus(200)
      .inspectRequest()
      .inspectJSON()
    .toss();

    //
    // DONT KNOW where this endpoint is?  userpass/new??
    // but we need it
    frisby.create('create a brand new user')
      .get(base_url + '/api/users/new', { strictSSL: false })
      .expectStatus(201)
    
      // .inspectRequest()
      // .inspectJSON()
    .toss();


    //
    // THESE TESTS NEED TO BE WRITTEN
    //
    
    // frisby.create('upload a file test')
    //   .post(base_url + '/api/projects/{project_id}/export', { title: 'a test project' },  { strictSSL: false })
    //   // write the test
    // .toss();

    // frisby.create('delete a file test')
    //   // write the test
    // .toss();
    
    // frisby.create('Request Content')
    //   .post(base_url + '/api/users/request-content', { title: 'a test project' },  { strictSSL: false })
    //   // write the test
    // .toss();

  
    //*****//
    //  end tests that require a token
    //*****//
    
  }) // end after json
  
.toss();
//*****//
//  THE FINAL END TOSS FOR AN AUTH TOKEN
//*****//


