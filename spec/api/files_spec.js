var frisby = require('frisby');

/* Config Options with defaults */
var emaillogin = process.env['emaillogin'];
var password = process.env['password'];
var base_url = process.env['base_url'];

// this is for future work.  Right now we grab the first project in the user's list of projects for the test
var a_project_id = process.env['project_id'];


if (base_url == null){  
  base_url = 'https://test.contiq.com';
}
if (emaillogin == null){  
    emaillogin = 'graham@chiseldesign.com';
}
if (password == null){  
  password = 'grahammyhre';
}
/* end config options with defaults */

frisby.create('Test login and get an Auth Token for all other tests')
  .post(base_url + '/api/authenticate/userpass?username=' + emaillogin + '&password=' + password +'', { some: 'test data' }, { strictSSL: false })
  //.inspectRequest()
  .inspectJSON()
  .expectStatus(200)
  .expectHeaderContains('content-type', 'application/json')
  // examples
  // .expectJSON('0', {
  //   posts: function(val) { expect(val).toMatchOrBeNull("Oklahoma City, OK"); }, // Custom matcher callback
  //   user: {
  //     verified: false,
  //     location: "Oklahoma City, OK",
  //     url: "http://brightb.it"
  //   }
  // })
  // .expectJSON({
  //     token:
  //     {
  //       title: "image from : 64 - brad@betterbrushes.com"
  //     }
  //   })
  .expectJSONTypes({
    token: String,
    expiresOn: String 
  })
  .afterJSON(function(response) {
    console.log("\n did we get a response auth token? :: " + response.token); // TESTING
    
    // 
    // Include API KEY in the header of all future requests
    //
    frisby.globalSetup({
    request: {
      headers: {
        'X-Auth-Token': response.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        // add a ten second timeout for the export function
        
       }
     }
   });
   

   
	//***********************************************************************/
	//FILES tests, this is new endpoint possibly slowly replacing /resources/
	//***********************************************************************/

	//************
	//* GET FILES *
	//************/
	
	frisby.create('Get users files;no query parameters')
		.get(base_url + '/api/files', { strictSSL: false })
		.expectStatus(200)
		.expectJSONTypes('paging', {
			page: Number,
			numberOfPages: Number,
			itemsPerPage: Number
		})
	.toss();
	
	frisby.create('Check if page parameter is correct')
		.get(base_url + '/api/files?page=1', { strictSSL: false })
		.expectStatus(200)
		.expectJSON('paging', {
			page: 1
		})
	.toss();
	
	frisby.create('Check if page parameter is corrected to zero')
		.get(base_url + '/api/files?page=-11', { strictSSL: false })
		.expectStatus(200)
		.expectJSON('paging', {
			page: 0
		})
	.toss();
   
    
  }) // end after json
  
.toss();
//*****//
//  THE FINAL END TOSS FOR AN AUTH TOKEN
//*****//


