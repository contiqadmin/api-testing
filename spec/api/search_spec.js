var frisby = require('frisby');

/* Config Options with defaults */
var emaillogin = process.env['emaillogin'];
var password = process.env['password'];
var base_url = process.env['base_url'];

// this is for future work.  Right now we grab the first project in the user's list of projects for the test
var a_project_id = process.env['project_id'];


if (base_url == null){  
  base_url = 'https://test.contiq.com';
}
if (emaillogin == null){  
    emaillogin = 'graham@chiseldesign.com';
}
if (password == null){  
  password = 'grahammyhre';
}
/* end config options with defaults */

frisby.create('Test login and get an Auth Token for all other tests')
  .post(base_url + '/api/authenticate/userpass?username=' + emaillogin + '&password=' + password +'', { some: 'test data' }, { strictSSL: false })
  //.inspectRequest()
  .inspectJSON()
  .expectStatus(200)
  .expectHeaderContains('content-type', 'application/json')
  .expectJSONTypes({
    token: String,
    expiresOn: String 
  })
  .afterJSON(function(response) {
    console.log("\n did we get a response auth token? :: " + response.token); // TESTING
    
    // 
    // Include API KEY in the header of all future requests
    //
    frisby.globalSetup({
    request: {
      headers: {
        'X-Auth-Token': response.token,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        // add a ten second timeout for the export function
        
       }
     }
   });
   
   
	//***********************************************************************/
	// TEST THE SEARCH FUNCTIONALITY /
	//***********************************************************************/
	frisby.create('Test the search functionality.')
		.get(base_url + '/api/crs/search?query=ROI', { strictSSL: false })
		.expectStatus(200)
		.afterJSON(function(obj){
			expect(obj.data.docs.length > 0).toBe(true);
		})
		 .expectJSONTypes('data.docs.0', {
			cei: Number,
			title: String,
			thumbnail_url: String,
			number: Number,
			visual: Number,
			text: String,
			fileid: String,
			crs: Number,
			author: {
				firstName: String,
				lastName: String
			},
			cwordcount: Number,
			"files-count": Number,
			relevance: Number,
			filename: String,
			img_url: String,
			"slides-count": Number,
			slide_download_url: String,
			lastUpdateTime: Number
		})
	.toss();
   
    
  }) // end after json
  
.toss();
//*****//
//  THE FINAL END TOSS FOR AN AUTH TOKEN
//*****//


