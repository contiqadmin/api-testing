# README #

A Sanity check for the contiq API

### What is this repository for? ###

* This script does some basic tests of the API to ensure it is running and the baseline methods are functioning
* Version 0.1

These tests will run against 
http://50.112.128.109:8000
unless configured at cli


### How do I get set up? ###

* Summary of set up
This is the lightest weight testing framework I could find that works well.  

Frisby requires both node.js and NPM to be installed on your system, and is installable as an NPM package. To install, run in your module

Step 1:
npm install --save-dev frisby

Step 2:
npm install -g jasmine-node

Step 3:
cd your/checked/out/repo/project
jasmine-node spec/api/


more at http://frisbyjs.com/  they have very simple docs!

* Configuration

There is configuration from the cli with defaults

example 1
simple
jasmine-node spec/api/jasmine-node spec/api/

example 2
with configuration
base_url is full baseurl with port
 jasmine-node --config emaillogin youremaillogin --config password yourpassword --config base_url http://50.112.128.109:8000 spec/api/sanity_check_spec.js

* Dependencies
1.  Node
2.  Jasmine
3.  Frisby

Also, this test requires data to already be loaded into the system you are testing.
We should add something to test and load an empty system too.


* How to run tests

run jasmine-node spec/api/ from cli
ex.
Grahams-MacBook-Pro:contiq-tests grahammyhre$ jasmine-node spec/api/


* Deployment instructions
TBD where we can deploy as part of continuous integration

### Contribution guidelines ###

* PLEASE contribute tests.  We need more :) :)

### Who do I talk to? ###
Some helpful links I found while setting this up
for auth variables
https://ptmccarthy.github.io/2014/06/28/rest-testing-with-frisby/

for passing in configs
http://bazscott.com/blog/2014/11/25/API-Testing-With-Frisby/

